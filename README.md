# PDF Server

Server-side PDF generation using PDFMake

## Credits
PDF Server was developed by Matt Yoder for the [Champaign County
Regional Planning Commission][1].

## License
PDF Server is available under the terms of the [BSD 3-clause license][2].

[1]: https://ccrpc.org/
[2]: https://gitlab.com/ccrpc/pdf-server/blob/master/LICENSE.md
