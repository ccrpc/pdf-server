const fs = require('fs');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const PdfServer = require('./server.js');


async function getConfig() {
  if (process.argv.length > 2) {
    let configString = await readFile(process.argv[2]);
    return JSON.parse(configString);
  }
  return {};
}

async function startServer() {
  let config = await getConfig();
  let server = new PdfServer(config);
  server.start();
}

if (require.main === module) {
  startServer();
}
