const bodyParser = require('body-parser');
const express = require('express');
const fs = require('fs');
const { spawn } = require('child_process');
const tmp = require('tmp');
const PdfPrinter = require('pdfmake');


class PdfServer {
  constructor(config) {
    this.defaultTemplate = config.defaultTemplate;
    this.styles = config.styles || {};
    this.templates = config.templates;
    this.fonts = config.fonts || {};
    this.port = config.port || 8000;
    this.app = express();
    this.printer = new PdfPrinter(this.fonts);

    if (config.referrers)
      this.app.use(this.referrerMiddleware(config.referrers));
    this.app.use(bodyParser.json({limit: config.bodySizeLimit || '1mb'}));
    this.app.post('/pdf/:filename', (req, res) => this.pdfPost(req, res));
  }

  referrerMiddleware(referrers) {
    let refExps = referrers.map((ref) => new RegExp(ref, 'i'));
    return (req, res, next) => {
      let refHeader = req.get('Referer');
      if (refHeader)
        for (let i = 0; i < refExps.length; i++)
          if (refHeader.match(refExps[i])) return next();
      res.status('403').send('Forbidden');
    }
  }

  sendUnderlay(template, pdfDoc, req, res) {
    let settings = template.underlay;
    let args = ['--underlay', settings.file];
    if (settings.repeat) args.push(`--repeat=${settings.repeat}`);
    args.push('--');

    tmp.file((err, path, fd, cleanupCallback) => {
      if (err) throw err;
      args.push(path, '-');

      let writeStream = pdfDoc.pipe(fs.createWriteStream(path));
      pdfDoc.end();

      writeStream.on('finish', () => {
        let qpdf = spawn('qpdf', args);

        qpdf.stderr
          .once('data', () => res.status(500).send('Error applying underlay'))
          .pipe(process.stderr);

        qpdf.stdout
          .once('data', () => this.setHeaders(req, res))
          .pipe(res);

        qpdf.on('exit', () => cleanupCallback());
      });
    });
  }

  getTemplate(req) {
    return this.templates[req.body.template || this.defaultTemplate] || {};
  }

  getDoc(req, template) {
    let docDef = {
      ...req.body,
      ...(template.document || {})
    };

    docDef.styles = {
      ...(this.styles || {}),
      ...(docDef.styles || {})
    };

    if (template.defaultStyle)
      docDef.defaultStyle = this.styles[template.defaultStyle];

    return this.printer.createPdfKitDocument(docDef, {});
  }

  setHeaders(req, res) {
    res.contentType('application/pdf');
    res.setHeader(
      'Content-Disposition',
      `attachment; filename="${req.params.filename}"`);
  }

  pdfPost(req, res) {
    let template = this.getTemplate(req);
    let pdfDoc = this.getDoc(req, template);
    if (template.underlay) {
      this.sendUnderlay(template, pdfDoc, req, res);
    } else {
      this.setHeaders(req, res);
      pdfDoc.pipe(res);
      pdfDoc.end();
    }
  }

  start() {
    this.app.listen(this.port, () => {
      console.log(`Listening on port ${this.port}`);
    });
  }
}

module.exports = PdfServer;
