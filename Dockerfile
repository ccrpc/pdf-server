FROM alpine:3.10

RUN apk add --no-cache \
  nodejs \
  npm \
  qpdf \
  yarn

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
COPY yarn.lock /usr/src/app/
RUN yarn install
COPY ./src /usr/src/app/src

EXPOSE 8000
VOLUME /etc/pdf

CMD [ "npm", "start", "--", "/etc/pdf/config.json" ]
